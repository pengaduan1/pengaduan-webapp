import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Pengaduan } from '../models/pengaduan';
import { DataTablesRequest } from '../models/datatablesrequest.model';
import { DataTablesResponse } from '../models/datatablesresponse.model';

@Injectable()
export class PengaduanService{
    constructor(private httpKlien: HttpClient){

    }
    listpengaduan( ): Observable<Pengaduan[]> {
        return this.httpKlien.get(environment.baseUrl + '/listpengaduanjson')
        .pipe(map(data => data as Pengaduan[]));
    }

    insertPengaduan(pengaduan: Pengaduan): Observable<any> {
        return this.httpKlien.post(environment.baseUrl + '/savepengaduanjson' , pengaduan)
        .pipe(map(data => data));
    }

    gantiStatus(pengaduan: Pengaduan): Observable<any> {
      return this.httpKlien.post(environment.baseUrl + '/gantistatuspengaduan' , pengaduan)
        .pipe(map(data => data));
    }

    // Get Datatables
    getListPengaduanAll(parameter: Map<string, any>, dataTablesParameters: any): Observable<DataTablesResponse> {
      const dtReq = new DataTablesRequest();
      dtReq.draw = dataTablesParameters.draw;
      dtReq.length = dataTablesParameters.length;
      dtReq.start = dataTablesParameters.start;
      dtReq.sortCol = dataTablesParameters.order[0].column;
      dtReq.sortDir = dataTablesParameters.order[0].dir;
      dtReq.extraParam = {};

      parameter.forEach((value, key) => {
        // @ts-ignore
        dtReq.extraParam[key] = value;
      });
      return this.httpKlien.post(environment.baseUrl + '/listpengaduandatajson', dtReq
      ).pipe(map(data => data as DataTablesResponse));
    }

}
