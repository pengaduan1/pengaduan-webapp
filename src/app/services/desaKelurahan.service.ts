import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {DesaKelurahan} from '../models/desaKelurahan';

@Injectable()
export class DesaKelurahanService{
  constructor(private httpKlien: HttpClient){

  }

  listdesakelurahan( ): Observable<DesaKelurahan[]> {
    return this.httpKlien.get(environment.baseUrl + '/listdesakelurahanjson')
      .pipe(map(data => data as DesaKelurahan[]));
  }

  listdesakelurahanbykec({id}: { id: any }): Observable<DesaKelurahan>{
    return this.httpKlien.get(environment.baseUrl + '/listdesakelurahanjson/' + id)
      .pipe(map(data => data as DesaKelurahan));
  }

}
