import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {KategoriPengaduan} from '../models/kategoriPengaduan';

@Injectable()
export class KategoriPengaduanService{

  constructor(private httpKlien: HttpClient){

  }

  listkategoripengaduan( ): Observable<KategoriPengaduan[]> {
    return this.httpKlien.get(environment.baseUrl + '/listkategoripengaduanjson')
      .pipe(map(data => data as KategoriPengaduan[]));
  }

}
