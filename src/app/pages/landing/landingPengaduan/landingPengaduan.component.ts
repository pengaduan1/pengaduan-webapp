import { Component, OnInit } from '@angular/core';
import {KategoriPengaduan} from '../../../models/kategoriPengaduan';
import {Kecamatan} from '../../../models/kecamatan';
import {DesaKelurahan} from '../../../models/desaKelurahan';
import {PengaduanService} from '../../../services/pengaduan.service';
import {KategoriPengaduanService} from '../../../services/kategoriPengaduan.service';
import {KecamatanService} from '../../../services/kecamatan.service';
import {DesaKelurahanService} from '../../../services/desaKelurahan.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {FormGroup} from '@angular/forms';
declare let $: any;

@Component({
  selector: 'app-landing-pengaduan',
  templateUrl: './landingPengaduan.component.html',
  styleUrls: ['./landingPengaduan.component.scss'],
  providers: [PengaduanService, KategoriPengaduanService, KecamatanService, DesaKelurahanService]
})
export class LandingPengaduanComponent implements OnInit {

  listKategori?: KategoriPengaduan[];
  listKecamatan?: Kecamatan[];
  listDesaKelurahan?: DesaKelurahan[];
  form?: FormGroup;

  constructor(private pengaduanService: PengaduanService,
              private kategoriService: KategoriPengaduanService,
              private kecamatanService: KecamatanService,
              private desaKelurahanService: DesaKelurahanService,
              private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient) {

    // List Kategori Pengaduan
    this.kategoriService.listkategoripengaduan().subscribe((data) => {
      console.log(data);
      this.listKategori = data;
    }, error => {
          console.log(error);
    });

    // List Kecamatan
    this.kecamatanService.listkecamatan().subscribe((data) => {
      console.log(data);
      this.listKecamatan = data;
    }, error => {
      console.log(error);
    });

    // List Desa Kelurahan
    this.desaKelurahanService.listdesakelurahan().subscribe((data) => {
      console.log(data);
      this.listDesaKelurahan = data;
    }, error => {
      console.log(error);
    });

  }

  ngOnInit(): void {

    // Select2 Kategori
    $('.select2').select2();
    // tslint:disable-next-line:only-arrow-functions typedef
    $('.select2').on('change', function() {
      // tslint:disable-next-line:prefer-const
      let data = $('.select2 option:selected').val();
      $('#test').val(data);
      console.log(data);
    });

    // Select2 Kecamatan
    $('.select2kecamatan').select2();
    // tslint:disable-next-line:only-arrow-functions typedef
    $('.select2kecamatan').on('change', function() {
      // tslint:disable-next-line:prefer-const
      let data = $('.select2kecamatan option:selected').val();
      $('#test').val(data);
      console.log(data);
    });

    // Select2 Desa Kelurahan
    $('.select2desakelurahan').select2();
    // tslint:disable-next-line:only-arrow-functions typedef
    $('.select2desakelurahan').on('change', function() {
      // tslint:disable-next-line:prefer-const
      let data = $('.select2desakelurahan option:selected').val();
      $('#test').val(data);
      console.log(data);
    });
  }

  getDesaKelurahan(): void {
    // @ts-ignore
    const idKecamatan = this.form.get('idKecamatan').value;
    this.desaKelurahanService.listdesakelurahanbykec(idKecamatan).subscribe( data => {
      // @ts-ignore
      this.listDesaKelurahan = data;
    });
  }

}
