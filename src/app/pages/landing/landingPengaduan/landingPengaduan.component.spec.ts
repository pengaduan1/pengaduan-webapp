import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPengaduanComponent } from './landingPengaduan.component';

describe('PengaduanComponent', () => {
  let component: LandingPengaduanComponent;
  let fixture: ComponentFixture<LandingPengaduanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPengaduanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingPengaduanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
