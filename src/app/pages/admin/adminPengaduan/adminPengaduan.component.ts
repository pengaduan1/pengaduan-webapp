import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pengaduan } from '../../../models/pengaduan';
import { PengaduanService } from '../../../services/pengaduan.service';
import {HttpClient} from '@angular/common/http';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-pengaduan',
  templateUrl: './adminPengaduan.component.html',
  styleUrls: ['./adminPengaduan.component.scss'],
  providers: [PengaduanService]
})
export class AdminPengaduanComponent implements OnDestroy, OnInit {

  dtOptions: DataTables.Settings = {};
  listPengaduan: Pengaduan[] = [];
  dtTrigger: Subject<any> = new Subject<any>();

  constructor(private pengaduanService: PengaduanService,
              private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient) {

    // this.pengaduanService.listpengaduan().subscribe((data) => {
    //   console.log(data);
    //   this.listPengaduan = data;
    // }, error => {
    //       console.log(error);
    // });

  }

  ngOnInit(): void {


    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      dom: 'Bfrtip',
      // @ts-ignore
      buttons: [
        'copy',
        'print',
        'excel',
        'csv',
        'pdf'
      ]
    };
    this.pengaduanService.listpengaduan()
      .subscribe(data => {
        console.log(data);
        this.listPengaduan = data;
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      });

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}
