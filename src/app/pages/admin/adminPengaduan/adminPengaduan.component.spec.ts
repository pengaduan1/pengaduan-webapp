import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPengaduanComponent } from './adminPengaduan.component';

describe('PengaduanComponent', () => {
  let component: AdminPengaduanComponent;
  let fixture: ComponentFixture<AdminPengaduanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminPengaduanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPengaduanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
