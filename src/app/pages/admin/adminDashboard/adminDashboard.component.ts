import { Component, OnInit } from '@angular/core';
import {PengaduanService} from '../../../services/pengaduan.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './adminDashboard.component.html',
  styleUrls: ['./adminDashboard.component.scss'],
  providers: [PengaduanService]
})
export class AdminDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
