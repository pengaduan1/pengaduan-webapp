import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LingkunganHidupComponent } from './lingkungan-hidup.component';

describe('LingkunganHidupComponent', () => {
  let component: LingkunganHidupComponent;
  let fixture: ComponentFixture<LingkunganHidupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LingkunganHidupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LingkunganHidupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
