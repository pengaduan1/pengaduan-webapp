import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportasiComponent } from './transportasi.component';

describe('TransportasiComponent', () => {
  let component: TransportasiComponent;
  let fixture: ComponentFixture<TransportasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransportasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
