import {AfterViewInit, Component, OnInit} from '@angular/core';
import '@grapecity/wijmo.styles/wijmo.css';

import * as wjcCore from '@grapecity/wijmo';
import * as wjcXlsx from '@grapecity/wijmo.xlsx';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-transportasi',
  templateUrl: './transportasi.component.html',
  styleUrls: ['./transportasi.component.scss']
})
export class TransportasiComponent implements OnInit, AfterViewInit {

  constructor() { }

  workbook?: wjcXlsx.Workbook;
  sheetIndex?: number;

  ngAfterViewInit(): void {
    // @ts-ignore
    document.getElementById('importFile').addEventListener('change', () => {
      this._loadWorkbook();
    });
  }

  // tslint:disable-next-line:typedef
  tabClicked(e: MouseEvent, index: number) {
    e.preventDefault();
    this._drawSheet(index);
  }

  // tslint:disable-next-line:typedef
  private _loadWorkbook() {
    const reader = new FileReader();
    //
    reader.onload = (e) => {
      const workbook = new wjcXlsx.Workbook();
      // @ts-ignore
      workbook.loadAsync(reader.result, (result: wjcXlsx.Workbook) => {
        this.workbook = result;
        this._drawSheet(this.workbook.activeWorksheet || 0);
      });
    };
    //
    // @ts-ignore
    const file = (document.getElementById('importFile') as HTMLInputElement).files[0];
    if (file) {
      reader.readAsDataURL(file);
    }
  }

  // tslint:disable-next-line:typedef
  private _drawSheet(sheetIndex: number) {
    const drawRoot = document.getElementById('tableHost');
    // @ts-ignore
    drawRoot.textContent = '';
    this.sheetIndex = sheetIndex;
    // @ts-ignore
    this._drawWorksheet(this.workbook, sheetIndex, drawRoot, 200, 100);
  }

  // tslint:disable-next-line:typedef
  private _drawWorksheet(workbook: wjcXlsx.IWorkbook, sheetIndex: number, rootElement: HTMLElement, maxRows: number, maxColumns: number) {
    // NOTES:
    // Empty cells' values are numeric NaN, format is "General"
    //
    // Excessive empty properties:
    // fill.color = undefined
    //
    // netFormat should return '' for ''. What is 'General'?
    // font.color should start with '#'?
    // Column/row styles are applied to each cell style, this is convenient, but Column/row style info should be kept,
    // for column/row level styling
    // formats conversion is incorrect - dates and virtually everything; netFormat - return array of formats?
    // ?row heights - see hello.xlsx
    // tslint:disable-next-line:triple-equals
    if (!workbook || !workbook.sheets || sheetIndex < 0 || workbook.sheets.length == 0) {
      return;
    }
    //
    sheetIndex = Math.min(sheetIndex, workbook.sheets.length - 1);
    //
    if (maxRows == null) {
      maxRows = 200;
    }
    //
    if (maxColumns == null) {
      maxColumns = 100;
    }
    //
    // Namespace and XlsxConverter shortcuts.
    // tslint:disable-next-line:one-variable-per-declaration
    const sheet = workbook.sheets[sheetIndex],
      defaultRowHeight = 20,
      defaultColumnWidth = 60,
      tableEl = document.createElement('table');
    //
    tableEl.border = '1';
    tableEl.style.borderCollapse = 'collapse';
    //
    let maxRowCells = 0;
    for (let r = 0; sheet.rows && r < sheet.rows.length; r++) {
      if (sheet.rows[r]) {
        if (sheet.rows[r].cells) {
          // @ts-ignore
          maxRowCells = Math.max(maxRowCells, sheet.rows[r].cells.length);
        }
      }
    }
    //
    // add columns
    let columnCount = 0;
    if (sheet.columns) {
      columnCount = sheet.columns.length;
      maxRowCells = Math.min(Math.max(maxRowCells, columnCount), maxColumns);
      for (let c = 0; c < maxRowCells; c++) {
        // tslint:disable-next-line:one-variable-per-declaration
        const col = c < columnCount ? sheet.columns[c] : null,
          colEl = document.createElement('col');
        tableEl.appendChild(colEl);
        let colWidth = defaultColumnWidth + 'px';
        if (col) {
          // @ts-ignore
          this._importStyle(colEl.style, col.style);
          if (col.autoWidth) {
            colWidth = '';
          } else if (col.width != null) {
            colWidth = col.width + 'px';
          }
        }
        colEl.style.width = colWidth;
      }
    }
    //
    // generate rows
    // @ts-ignore
    const rowCount = Math.min(maxRows, sheet.rows.length);
    for (let r = 0; sheet.rows && r < rowCount; r++) {
      // tslint:disable-next-line:one-variable-per-declaration
      const row = sheet.rows[r],
        rowEl = document.createElement('tr');
      tableEl.appendChild(rowEl);
      if (row) {
        // @ts-ignore
        this._importStyle(rowEl.style, row.style);
        if (row.height != null) {
          rowEl.style.height = row.height + 'px';
        }
        for (let c = 0; row.cells && c < row.cells.length; c++) {
          // tslint:disable-next-line:one-variable-per-declaration
          const cell = row.cells[c],
            cellEl = document.createElement('td');
          rowEl.appendChild(cellEl);
          if (cell) {
            // @ts-ignore
            this._importStyle(cellEl.style, cell.style);
            let value = cell.value;
            if (!(value == null || value !== value)) { // TBD: check for NaN should be eliminated
              // tslint:disable-next-line:triple-equals
              if (wjcCore.isString(value) && value.charAt(0) == '\'') {
                value = value.substr(1);
              }
              let netFormat = '';
              if (cell.style && cell.style.format) {
                netFormat = wjcXlsx.Workbook.fromXlsxFormat(cell.style.format)[0];
              }
              const fmtValue = netFormat ? wjcCore.Globalize.format(value, netFormat) : value;
              cellEl.innerHTML = wjcCore.escapeHtml(fmtValue);
            }
            if (cell.colSpan && cell.colSpan > 1) {
              cellEl.colSpan = cell.colSpan;
              c += cellEl.colSpan - 1;
            }
          }
        }
      }
      // pad with empty cells
      const padCellsCount = maxRowCells - (row && row.cells ? row.cells.length : 0);
      for (let i = 0; i < padCellsCount; i++) {
        rowEl.appendChild(document.createElement('td'));
      }
      //
      if (!rowEl.style.height) {
        rowEl.style.height = defaultRowHeight + 'px';
      }
    }
    //
    // do it at the end for performance
    rootElement.appendChild(tableEl);
  }

  // tslint:disable-next-line:typedef
  private _importStyle(cssStyle: CSSStyleDeclaration, xlsxStyle: wjcXlsx.IWorkbookStyle) {
    if (!xlsxStyle) {
      return;
    }
    //
    if (xlsxStyle.fill) {
      if (xlsxStyle.fill.color) {
        cssStyle.backgroundColor = xlsxStyle.fill.color;
      }
    }
    //
    // tslint:disable-next-line:triple-equals
    if (xlsxStyle.hAlign && xlsxStyle.hAlign != wjcXlsx.HAlign.Fill) {
      cssStyle.textAlign = wjcXlsx.HAlign[xlsxStyle.hAlign].toLowerCase();
    }
    //
    const font = xlsxStyle.font;
    if (font) {
      if (font.family) {
        cssStyle.fontFamily = font.family;
      }
      if (font.bold) {
        cssStyle.fontWeight = 'bold';
      }
      if (font.italic) {
        cssStyle.fontStyle = 'italic';
      }
      if (font.size != null) {
        cssStyle.fontSize = font.size + 'px';
      }
      if (font.underline) {
        cssStyle.textDecoration = 'underline';
      }
      if (font.color) {
        cssStyle.color = font.color;
      }
    }
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  submit(){

    setTimeout(() => {
      Swal.fire('Oops...', 'Tidak dapat terhubung!', 'error');
    }, 3000);

  }

}
