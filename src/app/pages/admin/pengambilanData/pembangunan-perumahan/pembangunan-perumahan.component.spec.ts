import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PembangunanPerumahanComponent } from './pembangunan-perumahan.component';

describe('PembangunanPerumahanComponent', () => {
  let component: PembangunanPerumahanComponent;
  let fixture: ComponentFixture<PembangunanPerumahanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PembangunanPerumahanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PembangunanPerumahanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
