import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../../../services/auth/register.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  // @ts-ignore
  registerForm: FormGroup;

  constructor(private registerService: RegisterService,
              private formBuilder: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({
      username : this.formBuilder.control(null),
      keyword : this.formBuilder.control(null)
    });

  }

  // tslint:disable-next-line:typedef
  saveUser(){
    // @ts-ignore
    const value = this.registerForm.value;
    this.registerService.saveUser(value).subscribe(response => {
      // tslint:disable-next-line:triple-equals
      if (response.status == 201){
        alert('Anda telah mendaftar');
        this.router.navigate(['/login']);
      }
    }, error => {
      alert('Cannot register');
    });
  }

}
