import {KategoriPengaduan} from './kategoriPengaduan';
import {Kecamatan} from './kecamatan';
import {DesaKelurahan} from './desaKelurahan';

export class Pengaduan{

  idPengaduan?: string;
  nik?: string;
  namaLengkap?: string;
  noTelp?: string;
  deskripsi?: string;
  lokasiAduan?: string;
  foto?:string;
  status?: string;
  tanggal?: string;
  kategoriPengaduan?: KategoriPengaduan;
  kecamatan?: Kecamatan;
  desaKelurahan?: DesaKelurahan;
  idKategoriPengaduan?: string;
  namaKategoriPengaduan?: string;
  idKecamatan?: string;
  namaKecamatan?: string;
  idDesaKelurahan?: string;
  namaDesaKelurahan?: string;


}
