import {Kecamatan} from './kecamatan';

export class DesaKelurahan{

  idDesaKelurahan?: string;
  namaDesaKelurahan?: string;
  kecamatan?: Kecamatan;
  idKecamatan?: string;
  namaKecamatan?: string;


}
