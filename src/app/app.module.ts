import { BrowserModule } from '@angular/platform-browser';
import {enableProdMode, LOCALE_ID, NgModule} from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgSelect2Module } from 'ng-select2';
import { DataTablesModule } from 'angular-datatables';
import { registerLocaleData } from '@angular/common';
import localeId from '@angular/common/locales/id';

registerLocaleData(localeId, 'id');

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminPengaduanComponent } from './pages/admin/adminPengaduan/adminPengaduan.component';
import { AdminDashboardComponent } from './pages/admin/adminDashboard/adminDashboard.component';
import { AdminSidebarComponent } from './pages/admin/adminLayouts/adminSidebar/adminSidebar.component';
import { AdminNavbarComponent } from './pages/admin/adminLayouts/adminNavbar/adminNavbar.component';
import { AdminFooterComponent } from './pages/admin/adminLayouts/adminFooter/adminFooter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LandingSidebarComponent } from './pages/landing/landingLayouts/landing-sidebar/landing-sidebar.component';
import { LandingNavbarComponent } from './pages/landing/landingLayouts/landing-navbar/landing-navbar.component';
import { LandingFooterComponent } from './pages/landing/landingLayouts/landing-footer/landing-footer.component';
import {LandingDashboardComponent} from './pages/landing/landingDashboard/landingDashboard.component';
import {LandingPengaduanComponent} from './pages/landing/landingPengaduan/landingPengaduan.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { LoginComponent } from './pages/auth/login/login.component';
import {AuthService} from './auth.service';
import {AuthGuardService} from './authGuard.service';
import { TransportasiComponent } from './pages/admin/pengambilanData/transportasi/transportasi.component';
import { LingkunganHidupComponent } from './pages/admin/pengambilanData/lingkungan-hidup/lingkungan-hidup.component';
import { PembangunanPerumahanComponent } from './pages/admin/pengambilanData/pembangunan-perumahan/pembangunan-perumahan.component';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { ResultComponent } from './pages/admin/pengambilanData/result/result.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminPengaduanComponent,
    AdminDashboardComponent,
    AdminSidebarComponent,
    AdminNavbarComponent,
    AdminFooterComponent,
    LandingSidebarComponent,
    LandingNavbarComponent,
    LandingFooterComponent,
    LandingDashboardComponent,
    LandingPengaduanComponent,
    RegisterComponent,
    LoginComponent,
    TransportasiComponent,
    LingkunganHidupComponent,
    PembangunanPerumahanComponent,
    ResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgSelect2Module,
    DataTablesModule
  ],
  providers: [AuthService, AuthGuardService, {provide: LOCALE_ID, useValue: 'id-ID'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
enableProdMode();
// Bootstrap application with hash style navigation and global services.
platformBrowserDynamic().bootstrapModule(AppModule);
