import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './pages/admin/adminDashboard/adminDashboard.component';
import { AdminPengaduanComponent } from './pages/admin/adminPengaduan/adminPengaduan.component';
import {LandingDashboardComponent} from './pages/landing/landingDashboard/landingDashboard.component';
import {LandingPengaduanComponent} from './pages/landing/landingPengaduan/landingPengaduan.component';
import {RegisterComponent} from './pages/auth/register/register.component';
import {LoginComponent} from './pages/auth/login/login.component';
import {AuthGuardService} from './authGuard.service';
import {TransportasiComponent} from './pages/admin/pengambilanData/transportasi/transportasi.component';
import {LingkunganHidupComponent} from './pages/admin/pengambilanData/lingkungan-hidup/lingkungan-hidup.component';
import {PembangunanPerumahanComponent} from './pages/admin/pengambilanData/pembangunan-perumahan/pembangunan-perumahan.component';
import { ResultComponent } from './pages/admin/pengambilanData/result/result.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/index',
    pathMatch: 'full'
  },
  {
    path: 'index',
    component: LandingDashboardComponent
  },
  {
    path: 'pengaduan',
    component: LandingPengaduanComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },

  // Admin Page

  {
    path: 'admin-page',
    children: [
      {
        path: '',
        redirectTo: '/admin-page/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        // canActivate: [AuthGuardService],
        // data: {allowedRoles: ['role_user', 'role_admin', 'super_admin']},
        component: AdminDashboardComponent
      },
      {
        path: 'data-pengaduan',
        // canActivate: [AuthGuardService],
        // data: {allowedRoles: ['role_user', 'role_admin', 'super_admin']},
        component: AdminPengaduanComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'pengambilan-data',
        children: [
          {
            path: '',
            redirectTo: '/admin-page/dashboard',
            pathMatch: 'full'
          },
          {
            path: 'transportasi-dan-sumber-daya-air',
            // canActivate: [AuthGuardService],
            // data: {allowedRoles: ['role_user', 'role_admin', 'super_admin']},
            component: TransportasiComponent
          },
          {
            path: 'lingkungan-hidup-dan-tata-ruang',
            // canActivate: [AuthGuardService],
            // data: {allowedRoles: ['role_user', 'role_admin', 'super_admin']},
            component: LingkunganHidupComponent
          },
          {
            path: 'pembangunan-perumahan-dan-permukiman',
            // canActivate: [AuthGuardService],
            // data: {allowedRoles: ['role_user', 'role_admin', 'super_admin']},
            component: PembangunanPerumahanComponent
          },
          {
            path: 'result',
            // canActivate: [AuthGuardService],
            // data: {allowedRoles: ['role_user', 'role_admin', 'super_admin']},
            component: ResultComponent
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
