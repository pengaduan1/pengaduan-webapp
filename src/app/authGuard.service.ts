import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthGuardService implements CanActivate{

  constructor(private authService: AuthService, private router: Router){

  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean> | Promise<boolean> | boolean {
    const allowedRoles = next.data.allowedRoles;
    // console.log(this.authService.isAuthorized(allowedRoles));
    // @ts-ignore
    return this.authService.isAuthorized(allowedRoles)
      // @ts-ignore
      .pipe(map(data => {
        // @ts-ignore
        if (data.roles != null && allowedRoles.some(r => data.roles.includes(r)) && data.isValid){
          return true;
        }else{
          this.router.navigate(['/admin-page/login']);
        }
      }));
  }
}
